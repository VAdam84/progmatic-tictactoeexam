/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hp
 */
public class VictoryAwarePlayer extends AbstractPlayer {

    public VictoryAwarePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        Board board = b;
        ArrayList<Cell> cellsWithMyType = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                try {
                    if (board.getCell(i, j).equals(myType)) {
                        Cell cell = new Cell(i, j);
                        cellsWithMyType.add(cell);
                    }
                } catch (CellException ex) {
                    Logger.getLogger(VictoryAwarePlayer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }

        int rowNum = 0;
        int colNum = 0;
        Cell cellToPut = board.emptyCells().get(0);
        for (int i = 0; i < cellsWithMyType.size() - 1; i++) {
            if (cellsWithMyType.get(i).getCol() == cellsWithMyType.get(i + 1).getCol()) {

                colNum = cellsWithMyType.get(i).getCol();
            } else if (cellsWithMyType.get(i).getRow() == cellsWithMyType.get(i + 1).getRow()) {

                rowNum = cellsWithMyType.get(i).getRow();
            }
        }
        if (rowNum > 0) {
            for (Cell emptyCell : board.emptyCells()) {
                if (emptyCell.getRow() == rowNum) {
                    cellToPut = new Cell(emptyCell.getRow(), emptyCell.getCol(), myType);
                }
            }
        } else if (colNum > 0) {
            for (Cell emptyCell : board.emptyCells()) {
                if (emptyCell.getCol() == colNum) {
                    cellToPut = new Cell(emptyCell.getRow(), emptyCell.getCol(), myType);
                }
            }
        }
        return cellToPut;
    }

}
