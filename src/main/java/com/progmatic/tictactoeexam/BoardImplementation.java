package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;

import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

public class BoardImplementation implements Board {

    private int column = 3;
    private int row = 3;
    private PlayerType[][] board = new PlayerType[row][column];

    public BoardImplementation() {
        initBoard();
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public PlayerType[][] getBoard() {
        return board;
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        if (rowIdx < 0 || rowIdx >= board[0].length) {
            throw new CellException(rowIdx, colIdx, "The rowIdx is out of bounds of row(0-2)");
        }
        if (colIdx < 0 || colIdx >= board.length) {
            throw new CellException(rowIdx, colIdx, "The colIdx is out of bounds of row(0-2)");
        }
        return board[rowIdx][colIdx];

    }

    @Override
    public void put(Cell cell) throws CellException {

        if (cell.getCol() < 0 || cell.getCol() >= board.length || cell.getRow() < 0 || cell.getRow() >= board[0].length) {
            throw new CellException(cell.getRow(), cell.getCol(), "The cell column or row number out of Bounds");
        }
        if (board[cell.getCol()][cell.getRow()] != PlayerType.EMPTY) {
            throw new CellException(cell.getRow(), cell.getCol(), "The field of the board is not empty.");
        }

        board[cell.getCol()][cell.getRow()] = cell.getCellsPlayer();

//        
    }

    @Override
    public boolean hasWon(PlayerType p) {
        int diagonalLeftCount = 0;
        for (int i = 0; i < row; i++) {
            if ((board[i][0].equals(p)) && board[i][1].equals(p) && board[i][2].equals(p)) {
                return true;
            }
        }
        for (int i = 0; i < column; i++) {
            if (board[0][i].equals(p) && board[1][i].equals(p) && board[2][i].equals(p)) {
                return true;
            }
        }
        for (int i = 0; i < row; i++) {
            if (board[i][i].equals(p)) {
                diagonalLeftCount++;
            }
        }
        if (diagonalLeftCount == 3) {
            return true;
        }
        if (board[0][2].equals(p) && board[1][1].equals(p) && board[2][0].equals(p)) {
            return true;
        }
        return false;

    }

    @Override
    public List<Cell> emptyCells() {
        ArrayList<Cell> emptyCells = new ArrayList<>();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (board[i][j].equals(PlayerType.EMPTY)) {
                    Cell cell = new Cell(i, j);
                    emptyCells.add(cell);
                }
            }
        }
        return emptyCells;
    }

    public void initBoard() {

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                board[i][j] = PlayerType.EMPTY;
            }
        }
    }

    private Board fromString(String[][] aBoard) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
